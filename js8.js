const colorSelection = document.getElementById("colorSelection");
const fillSelection = document.getElementById("fillSelection");
const drawingBoard = document.getElementById("drawingBoard");
const sizeSelectionInput = document.getElementById("sizeSelection");
const clearBtn = document.getElementById("clearBtn");
const rgbInp = document.getElementById("rgbInp");

// sizeSelectionInput.addEventListener("input", () => {
//     sizeSelection = parseInt(sizeSelectionInput.value);
//     console.log(sizeSelection)
// });

rgbInp.oninput = function () {
  // removes any non number non comma character
  this.value = this.value.replace(/[^0-9,]/g, ""); // TODO: force format
};

// Associate color names to RGB values (tho not really needed since were using just simple colors )
let colors = {
  Red: "rgb(255, 0, 0)",
  White: "rgb(255, 255, 255)",
  Black: "rgb(0, 0, 0)",
};

drawingBoard.ondragstart = () => {
  //gets rid of the annoying dragging feature that sometimes messes up the mousedown value
  return false;
};

rgbInp.style.display = "none";
colorSelection.onchange = function () {
  //checks whats selected and hides/displays the input field as needed
  if (colorSelection.value === "RGB") {
    // theres a bug where if you aready selected RGB then reloaded the page the input bar is hidden until something else is selected and then back to rgb
    rgbInp.style.display = "inline";
  } else {
    rgbInp.style.display = "none";
  }
};

let isMouseDown = false; // A flag to track whether the mouse button is pressed
drawingBoard.onmousedown = () => {
  isMouseDown = true;
};
drawingBoard.onmouseup =  () => {
  isMouseDown = false;
};

function paintSquare(square, selectedColor) {
  // checkes if RGB vlue is selcted
  if (colorSelection.value === "RGB") {
    square.style.backgroundColor = `rgb(${rgbInp.value})`;
  } else {
    square.style.backgroundColor = colors[selectedColor];
  }
}

// The drawingBoard squares will be stored in this table for later use:
let squares = [];

// Create the drawingBoard squares:
for (let i = 0; i < 600; i++) {
  const square = document.createElement("div");

  square.style.height = "20px";
  square.style.width = "20px";
  square.style.float = "left";
  square.style.backgroundColor = colors["White"]; 

  clearBtn.onclick = () => {
    // clears everything by coloring everything white
    squares.forEach((element) => (element.style.backgroundColor =  colors["White"]));
  };

  square.onmousemove = () => {
    if (isMouseDown) {
      paintSquare(square, colorSelection.value);
      if (fillSelection.value === "Fill") {
        squares.forEach((element) =>
          paintSquare(element, colorSelection.value)
        );
      }
    }
  };

  // Add square as child to drawingBoard element
  drawingBoard.appendChild(square);
  // Add square to squares table
  squares.push(square);
  //  console.log(squares)
}
